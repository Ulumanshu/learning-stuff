from datetime import datetime
start = datetime.now()
end = datetime.now()
duration = end - start
print(start, end, duration)

planets = [{"num": 1,"radius": 2440000,"name": "Merkurijus","density": 5427},
           {"num": 2,"radius": 6052000,"name": "Venera","density": 5204},
           {"num": 3,"radius": 6378000,"name": "Zeme","density": 5513},
           {"num": 4,"radius": 3396000,"name": "Marsas","density": 3933}]