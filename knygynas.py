knygynas = {
    "books": [
        {"name": "Harry Potter", "author": "Kate Rowling", "year": "2000"},
        {"name": "Harry Potter 2", "author": "Kate Rowling", "year": "2003"},
        {"name": "Harry Potter 3", "author": "Kate Rowling", "year": "2005"},
        {"name": "Harry Potter 4", "author": "Kate Rowling", "year": "2007"},
        {"name": "Harry Potter 5", "author": "Kate Rowling", "year": "2009"},
        {"name": "Harry Potter 6", "author": "Kate Rowling", "year": "2012"},
        {"name": "Harry Potter 7", "author": "Kate Rowling", "year": "2015"},
        {"name": "Harry Potter 8", "author": "Kate Rowling", "year": "2017"},
        {"name": "Harry Potter 9", "author": "Kate Rowling", "year": "2019"},
        {"name": "Harry Potter 10", "author": "Kate Rowling", "year": "2022"},
        {"name": "Lord of The Rings", "author": "J. J. R. Tolkien", "year": "2000"},
        {"name": "Lord of The Rings 2", "author": "J. J. R. Tolkien", "year": "2001"},
        {"name": "Lord of The Rings 3", "author": "J. J. R. Tolkien", "year": "2020"},
        {"name": "Lord of The Rings 4", "author": "J. J. R. Tolkien", "year": "2023"},
        {"name": "Lord of The Rings 5", "author": "J. J. R. Tolkien", "year": "2045"},
        {"name": "Lord of The Rings 6", "author": "J. J. R. Tolkien", "year": "2060"},
        {"name": "Lord of The Rings 7", "author": "J. J. R. Tolkien", "year": "2097"},
        {"name": "Lord of The Rings 8", "author": "J. J. R. Tolkien", "year": "2099"},
        {"name": "Lord of The Rings 9", "author": "J. J. R. Tolkien", "year": "2102"},
        {"name": "Lord of The Rings 10", "author": "J. J. R. Tolkien", "year": "2103"},
        {"name": "Wolverine Principle", "author": "Clifford Simack", "year": "1982"},
        {"name": "Wolverine Principle 1", "author": "Clifford Simack", "year": "1984"},
        {"name": "Wolverine Principle 2", "author": "Clifford Simack", "year": "1987"},
        {"name": "Wolverine Principle 3", "author": "Clifford Simack", "year": "1991"},
        {"name": "Wolverine Principle 4", "author": "Clifford Simack", "year": "1995"},
        {"name": "Wolverine Principle 5", "author": "Clifford Simack", "year": "1999"},
        {"name": "Wolverine Principle 6", "author": "Clifford Simack", "year": "2000"},
        {"name": "Wolverine Principle 7", "author": "Clifford Simack", "year": "2001"},
        {"name": "Wolverine Principle 8", "author": "Clifford Simack", "year": "2006"},
        {"name": "Wolverine Principle 9", "author": "Clifford Simack", "year": "2009"},
        {"name": "Wolverine Principle 10", "author": "Clifford Simack", "year": "2011"},
        {"name": "Wolverine Principle 11", "author": "Clifford Simack", "year": "2017"},


    ]
}


def search(**kwargs):

    result = []
    xname = None
    xauthor = None
    xyear = None

    for key in kwargs.items():
        if key[0] == 'name':
            xname = key[1]
        elif key[0] == 'author':
            xauthor = key[1]
        elif key[0] == 'year':
            xyear = key[1]

    if xname and xauthor and xyear:
        for e in knygynas["books"]:
            if e["author"] == xauthor and e["year"] == xyear and e["name"] == xname:
                result.append(e)
                result.append('\\')

    elif xauthor and xyear and not xname:
        for e in knygynas["books"]:
            if e["author"] == xauthor and e["year"] == xyear:
                result.append(e)
                result.append('\\')

    elif xname and xyear and not xauthor:
        for e in knygynas["books"]:
            if e["year"] == xyear and e["name"] == xname:
                result.append(e)
                result.append('\\')

    elif xname and xauthor and not xyear:
        for e in knygynas["books"]:
            if e["author"] == xauthor and e["name"] == xname:
                result.append(e)
                result.append('\\')

    elif xauthor and not xyear and not xname:
        for e in knygynas["books"]:
            if e["author"] == xauthor:
                result.append(e)
                result.append('\\')

    elif xyear and not xauthor and not xname:
        for e in knygynas["books"]:
            if e["year"] == xyear:
                result.append(e)
                result.append('\\')

    elif xname and not xauthor and not xyear:
        for e in knygynas["books"]:
            if e["name"] == xname:
                result.append(e)
                result.append('\\')

    return result


def dictionizer(string):

    list = string.split(",")
    dict = {}

    for i in list:
        keyvalue = i.split(":")
        m = keyvalue[0]
        dict[m] = keyvalue[1]
    print(dict)

    return dict


def replacer(v):
    v1 = v.replace('\\', '\n')
    v2 = v1.replace("'", " ")
    v3 = v2.replace('[', ' ')
    v4 = v3.replace(']', ' ')
    v5 = v4.replace('{', '')
    v6 = v5.replace('}', '')
    v7 = v6.replace(",", "")

    return v7


x = input("Iveskite paieshkos zodzius be tarpu tarp simboliu formatu\n"
          "(name:pavadinimas,author:autorius,year:metai) :  ")
y = dictionizer(x)
resulto = str(search(**y))

print("Rezultatai:\n {}".format(replacer(resulto)))



