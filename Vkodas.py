n = input("Iveskite asmens koda: ")
n = list(n)
assert len(n) == 11, ('Asmens kodas yra is 11 skaitmenu')
n = [int(e) for e in n]
b = n[10]
o = n[0]


# a = ((n[0] * 1 + n[1] * 2 + n[2] * 3 + n[3] * 4 + n[4] * 5 + n[5] * 6 + n[6] * 7
#         + n[7] * 8 + n[8] * 9 + n[9] * 1))%11
# a1 = ((n[0] * 3 + n[1] * 4 + n[2] * 5 + n[3] * 6 + n[4] * 7 + n[5] * 8 + n[6] * 9
#         + n[7] * 1 + n[8] * 2 + n[9] * 3))%11

def method1(n):
    k = n[:10]
    i = 0
    s = 0
    for e in k:
        if i != 9:
            i += 1
            s += e * i
        else:
            del k[0:i]
            i -= 9
    for e in k:
        i += 1
        s += e * i
    return s % 11


def method2(n):
    k = n[:10]
    i = 2
    s = 0
    for e in k:
        if i != 9:
            i += 1
            s += e * i
        else:
            del k[0:i]
            i -= 9
    for e in k:
        i += 1
        s += e * i
    return s % 11


def method11(n):
    k = n[:10]
    s = 0
    for i, e in zip(range(len(k)), k):
        if i <= 8:
            s += e * (i + 1)
        elif i > 8:
            s += e * (i - 8)
    return s % 11


def method22(n):
    k = n[:10]
    s = 0
    for i, e in zip(range(len(k)), k):
        if i <= 6:
            s += e * (i + 3)
        elif i > 6:
            s += e * (i - 6)
    return s % 11


def replacer(v):
    v1 = v.replace(',', ' ')
    v2 = v1.replace("'", " ")
    v3 = v2.replace('[', ' ')
    v4 = v3.replace(']', ' ')
    v5 = v4.replace('  ', '')
    v6 = v5.replace(' ', '')
    return v6


a = method11(n)
a1 = method22(n)

vg = str(n)
vgs = 'A.k.:'
vgst = vgs + vg

if b == a:
    lo1 = "K1:{}".format(a)
    print(lo1, replacer(vgst))
elif b == a1:
    lo2 = "K2:{}".format(a1)
    print(lo2, replacer(vgst))
else:
    lo3 = "K1:{}, K2:{}".format(a, a1)
    print(lo3, replacer(vgst))

c = 0

if o == 9:
    print("Jusu asmens kodas nepatikrinamas")
    c = False
elif a != 10 and b == a:
    print("Asmens kodas teisingas")
    c = True
elif a1 != 10 and b == a1:
    print("Asmens kodas teisingas")
    c = True
elif b == 0:
    print("Asmens kodas teisingas")
    c = True
else:
    print("Asmens kodas neteisingas")
    c = False

if o == 1 or o == 3 or o == 5:
    lyt = 'Jus esate vyras: gimes'
elif o == 2 or o == 4 or o == 6:
    lyt = 'Jus esate moteris: gimusi'

if o == 1 or o == 2:
    ce = [1, 8]
elif o == 3 or o == 4:
    ce = [1, 9]
elif o == 5 or o == 6:
    ce = [2, 0]

m = ce + n[1:3]
m.append(':')
men = n[3:5]
men.append(':')
gd = n[5:7]
gdata = m + men + gd
enr = n[7:10]
v = str(gdata)
ka = str(enr)

if c:
    print(lyt, replacer(v))
    eilnrs = 'Jus buvote {} naujagimis tadien'.format(replacer(ka))
    print(eilnrs)