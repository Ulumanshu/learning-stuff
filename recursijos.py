sk = 1234
l = []
while sk > 0:
    l.append(sk % 10)
    sk = sk // 10

print(l)
print(sum(l))

sk = 1234
r = 0
while sk > 0:
    r += sk % 10
    sk //= 10

print(r)

def sksum(sk):
    m = sk // 10
    if m:
        return sk % 10 + sksum(m)
    else:
        return sk

print(sksum(1234))
