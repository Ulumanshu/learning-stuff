#moduliai
#import ir from moduliu itraukimui
# import modulio_pavadinimas [as kintamasis],[]
# import Mano_modulis as mano bus kintamasis moduliui kitu vardu
# import module1
#module1.printer("hello world")

#from module1 import printer
#printer("Hello world") "as" ira pervadinimo komanda * kopiojuoja visus modulio atributus su komanda\
#  from

# pirmo importo metu visas importuojamas modulis yra paleidziamas
#def, import, from yra vykdomieji sakiniai
#import module/ name1 = module.name1/ del module
#modulio vardus paziureti Modulisx dir(Modulisx), Modulis3._dict_
# kintamasis.path direktorijos kur jiesko daiktu from . import modulis jiesko modulio diresktorijoj
# _init_.py jei failas sitoj dir ji interpretatoriaus laikoma moduliu
#import doctest
#"""Doctest
#def t(n):
#>>>t(5)
#6
#>>>t(4)
#4
#"""
#return n+1

#doctest.testmod()