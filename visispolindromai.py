x = range(100,1000)
poli3 = [""]


def drom (x):
    r = 0
    while x > 0:
        r *= 10
        r += x % 10
        x //= 10
    return r


i = 0

for e in x:
    if e == drom(e):
        poli3.append(e)
        i += 1
    if i == 10:
        poli3.append("\\  +")
        i -= 10


msg = str(poli3)
msg = msg.translate(str.maketrans("+"," "))
msg = msg.translate(str.maketrans(","," "))
msg = msg.translate(str.maketrans("'"," "))
msg = msg.translate(str.maketrans("[,]"," , "))
msg = msg.translate(str.maketrans("\\","\n"))

print("Three digit polyndromes:\n", msg)


