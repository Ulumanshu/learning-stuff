knygynas = {
    "books": [
        {"name": "Harry Potter", "author": "Kate Rowling", "year": 2000, "theme": "Fantastika"},
        {"name": "Harry Potter 2", "author": "Kate Rowling", "year": 2003},
        {"name": "Harry Potter 3", "author": "Kate Rowling", "year": 2005},
        {"name": "Harry Potter 4", "author": "Kate Rowling", "year": 2007},
        {"name": "Harry Potter 5", "author": "Kate Rowling", "year": 2009},
        {"name": "Harry Potter 6", "author": "Kate Rowling", "year": 2012},
        {"name": "Harry Potter 7", "author": "Kate Rowling", "year": 2015},
        {"name": "Harry Potter 8", "author": "Kate Rowling", "year": 2017},
        {"name": "Harry Potter 9", "author": "Kate Rowling", "year": 2019},
        {"name": "Harry Potter 10", "author": "Kate Rowling", "year": 2022},
        {"name": "Lord of The Rings", "author": "J. J. R. Tolkien", "year": 2000},
        {"name": "Lord of The Rings 2", "author": "J. J. R. Tolkien", "year": 2001},
        {"name": "Lord of The Rings 3", "author": "J. J. R. Tolkien", "year": 2020},
        {"name": "Lord of The Rings 4", "author": "J. J. R. Tolkien", "year": 2023},
        {"name": "Lord of The Rings 5", "author": "J. J. R. Tolkien", "year": 2045},
        {"name": "Lord of The Rings 6", "author": "J. J. R. Tolkien", "year": 2060},
        {"name": "Lord of The Rings 7", "author": "J. J. R. Tolkien", "year": 2097},
        {"name": "Lord of The Rings 8", "author": "J. J. R. Tolkien", "year": 2099},
        {"name": "Lord of The Rings 9", "author": "J. J. R. Tolkien", "year": 2102},
        {"name": "Lord of The Rings 10", "author": "J. J. R. Tolkien", "year": 2103},
        {"name": "Wolverine Principle", "author": "Clifford Simack", "year": 1982},
        {"name": "Wolverine Principle 1", "author": "Clifford Simack", "year": 1984},
        {"name": "Wolverine Principle 2", "author": "Clifford Simack", "year": 1987},
        {"name": "Wolverine Principle 3", "author": "Clifford Simack", "year": 1991},
        {"name": "Wolverine Principle 4", "author": "Clifford Simack", "year": 1995},
        {"name": "Wolverine Principle 5", "author": "Clifford Simack", "year": 1999},
        {"name": "Wolverine Principle 6", "author": "Clifford Simack", "year": 2000},
        {"name": "Wolverine Principle 7", "author": "Clifford Simack", "year": 2001},
        {"name": "Wolverine Principle 8", "author": "Clifford Simack", "year": 2006},
        {"name": "Wolverine Principle 9", "author": "Clifford Simack", "year": 2009},
        {"name": "Wolverine Principle 10", "author": "Clifford Simack", "year": 2011},
        {"name": "Wolverine Principle 11", "author": "Clifford Simack", "year": 2017},


    ]
}

def get_all_keys(source):


    result = []
    for record in source:
        result.extend(record.keys())
    return set(result)

keys = get_all_keys(knygynas["books"])
print(keys)

def search_input(keys):


    result = {}
    listo = list(keys)

    for i, key in enumerate(keys, 1):
        print(i, key)

    cmd = -1
    print("pasirinkite ka ivesite arba 0 isejimui")

    while int(cmd) != 0:
        cmd = int(input("Iveskite rakta: "))
        try:
            cmd = int(cmd)
        except:
            cmd = -1
        if cmd != 0:
            key = listo[cmd-1]
            value = input("Ko ieshkote su raktu {}?: ".format(key))
            result[key] = value

    return result

searchq = search_input(keys)


def search_engine(library, searchq):


    results = []
    checklib = library
    true = 0
    false = 0
    count = 0
    for e in checklib:
        for key, value in searchq.items():
            if key in e:
                if str(e[key]) == value:
                    true = 1
                    count += 1
                elif str(e[key]) != value:
                    false = 1
                    count += 1
                if len(searchq.items()) == count and true == 1 and false == 0:
                    results.append(e)
                    results.append("\\")
                    true = 0
                    false = 0
                    count = 0
                elif len(searchq.items()) == count:
                    true = 0
                    false = 0
                    count = 0
            elif key not in e:
                false = 1
                count += 1
            elif len(searchq.items()) == count:
                true = 0
                false = 0
                count = 0

    return results


def replacer(v):
    v1 = v.replace('\\', '\n')
    v2 = v1.replace("'", " ")
    v3 = v2.replace('[', ' ')
    v4 = v3.replace(']', ' ')
    v5 = v4.replace('{', '')
    v6 = v5.replace('}', '')
    v7 = v6.replace(",", "")

    return v7

resultatas = replacer(str(search_engine(knygynas["books"], searchq)))

print("\n Paieshkos rezultatai: \n \n {}".format(resultatas))