#"Turis:%10.2f"%(((4 / 3) * a) * (self.r ** 3)) + " kub.m. "
#"Pavirsiaus plotas:%10.2f"%((4 * a) * (self.r ** 2)) + " kv.m. "
#print("Turis:%10.2f"%(sf1.Volume), "kub. m. ", "Pavirsiaus plotas:%10.2f"%(sf1.Surface), "kv. m. ")
#print("Turis:%10.2f"%(sf2.Volume), "kub. m. ", "Pavirsiaus plotas:%10.2f"%(sf2.Surface), "kv. m. ")

from timeit import default_timer as timer

from functools import wraps

from math import pi as a

print(a)

def mauglitime(orig):

    @wraps(orig)
    def wraper(*args, **kwargs):
        start = timer()
        r = orig(*args, **kwargs)
        end = timer()
        mins, secs = divmod(end - start, 60)
        mins = "%3.2f" % mins
        secs = "%3.7f" % secs
        print("Time elapsed:{} min. {} s.".format(mins, secs))
        print(orig.__name__)
        return r
    return wraper


def my_logger(orig_func):
        import logging
        logging.basicConfig(filename='{}.log'.format(orig_func.__name__), level=logging.INFO)

        @wraps(orig_func)
        def wrapper(*args, **kwargs):
            logging.info('Ran with args: {}, and kwargs: {}'.format(args, kwargs))
            return orig_func(*args, **kwargs)
        return wrapper


class Spheroid:
    "Class containing basic spheric objects"

    _count = 0

    #@mauglitime
    def __init__(self, num, radius):
        self._n = num
        self.r = radius
        Spheroid._count += 1   #counteris turi but inite

    def __repr__(self):
        return "Sferoidas Nr.: {}, spindulys {} m., vol.  {} m3, per. {} m2".format(self._n,
                self.r, self.Volume, self.Surface)

    @mauglitime
    @my_logger
    def __add__(self, other):
        return Spheroid(Spheroid._count + 1, self.r + other.r)

    def __lt__(self, other):
        return self.r < other.r

    @property
    @mauglitime
    def Volume(self):
        "Volume of the sphere"
        return ((4 / 3) * a) * (self.r ** 3)
    # Volume = property(Volume, doc= "sferos turis")

    @Volume.setter
    def Volume(self, volume):
        if volume > 0:
            r3 = volume / ((4/3) * a)
            self.r = r3 ** (1/3)
        elif volume == 0:
            self.r = 0
        else:
            raise ValueError("Neigiamas spindulys negalimas")

    @property
    def Surface(self):
        "Surface area of the sphere"
        return ((4 * a) * (self.r ** 2))

    @Surface.setter
    def Surface(self, surf):
        if surf > 0:
            r2 = surf / (4 * a)
            self.r = r2 ** (1/2)
        elif surf == 0:
            self.r = 0
        else:
            raise ValueError("Neigiamas spindulys negalimas")

sf1 = Spheroid(1,5)
sf1.Surface = 1017.9
print(sf1)
sf2 = Spheroid(2,9)
sf2.Volume = 523
print(sf2)
print(sf1 + sf2)
print("Jus dirbate su:%8i"%Spheroid._count,"sferiniais objektais")

if sf2 > sf1:
    print("sortas veikia")

planets = [{"num": 1,"radius": 2440000,"name": "Merkurijus","density": 5427},
           {"num": 2,"radius": 6052000,"name": "Venera","density": 5204},
           {"num": 3,"radius": 6378000,"name": "Zeme","density": 5513},
           {"num": 4,"radius": 3396000,"name": "Marsas","density": 3933}]

class Planetoid(Spheroid):
    "Contains extra variables for creation of celestial bodies:D"
    _count = 0

    def __init__(self, num, radius, name, density):
        super(Planetoid, self).__init__(num, radius)
        self.name = name
        self.den = density
        Planetoid._count += 1

    @property
    def Mass(self):
        "Calculates mass of planetoid"
        return self.Volume * self.den

    @property
    def Sgravity(self):
        "Calculates gravity in g's near surface"
        return (6.67 * 10**(-11)*self.Mass)/((self.r**2) * 9.8)


#for e in planets: e = Planetoid(**e),print(Planetoid(**e))
#e = Planetoid(4,999990,"O",5555)

p1 = Planetoid(**planets[0])
p2 = Planetoid(**planets[1])
p3 = Planetoid(**planets[2])
p4 = Planetoid(**planets[3])

print(p3)
print(p1.Sgravity)

print("Jus dirbate su:%8i"%Planetoid._count,"planetoidais")