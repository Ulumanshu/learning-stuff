sk = input(("Iveskite eiliute su skliaustais:"))

def nuskaitytuvas(sk):
    """Nuskaito skliaustus is bet kokio stringo:) teoriskai"""
    u = []
    a = "({[]})"
    for i in sk:
        if i in a:
            u.append(i)

    return (u)

skl = nuskaitytuvas(sk)
print(skl)

def tikrintuvas(skl):
    """Patikrina ar skliaustai gerai sudeti"""

    left = "({["
    right = ")}]"
    u = list(skl)
    k = []
    ern = 0

    if len(skl) % 2 != 0:
       x = "Error 1 - unclosed brackets"
       return x

    else:
        for e in u:
            if e in left:
                k.append(e)
                ern += 1
            elif e in right:
                if k and (k[-1] == "(" and e == ")"\
                          or k[-1] == "[" and e == "]"\
                          or k[-1] == "{" and e == "}"):
                    k.pop()
                    ern += 1
                else:
                 x = "Error 2 - wrong bracket type at index:",ern
                 return x

    if len(k) > 0 and len(skl) % 2 == 0:
      x = "Error 3 - wrong direction and/or type of bracket"
      return x

    elif len(skl) % 2 == 0 and len(k) == 0 and len(skl) == ern:
      x = "No Errors"
    return x


t = tikrintuvas(skl)
print(t)